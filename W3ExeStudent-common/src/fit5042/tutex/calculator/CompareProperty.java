package fit5042.tutex.calculator;

import javax.ejb.CreateException;
import javax.ejb.Remote;
import java.rmi.RemoteException;

import fit5042.tutex.repository.entities.Property;

@Remote
public interface CompareProperty {
	void addProperty(Property property);
	
	void removeProperty(Property property);
	
	int getBestPerRoom();
	
	// Create Method?
	CompareProperty create() throws CreateException, RemoteException;
}
