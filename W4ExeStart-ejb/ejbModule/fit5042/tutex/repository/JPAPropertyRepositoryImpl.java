package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.ContactPerson;
import fit5042.tutex.repository.entities.Property;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Eddie Leung
 */
@Stateless
public class JPAPropertyRepositoryImpl implements PropertyRepository {

    //insert code (annotation) here to use container managed entity manager to complete these methods 
	@PersistenceContext(unitName = "W4ExeStart-ejb")
    private EntityManager entityManager;

    @Override
    public void addProperty(Property property) throws Exception {
        List<Property> properties = entityManager.createNamedQuery(Property.GET_ALL_QUERY_NAME).getResultList();
        property.setPropertyId(properties.get(0).getPropertyId() + 1);
        entityManager.persist(property);
    }

    @Override
    public Property searchPropertyById(int id) throws Exception {
        Property property = entityManager.find(Property.class, id);
        property.getTags();
        return property;
    }

    @Override
    public List<Property> getAllProperties() throws Exception {
        return entityManager.createNamedQuery(Property.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public Set<Property> searchPropertyByContactPerson(ContactPerson contactPerson) throws Exception {
        contactPerson = entityManager.find(ContactPerson.class, contactPerson.getConactPersonId());
        contactPerson.getProperties().size();
        entityManager.refresh(contactPerson);

        return contactPerson.getProperties();
    }

    @Override
    public List<ContactPerson> getAllContactPeople() throws Exception {
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
    }

    @Override
    public void removeProperty(int propertyId) throws Exception {
        //complete this method
    	 Property property = this.searchPropertyById(propertyId);
         if (property != null) {
             entityManager.remove(property);
         }
    }

    @Override
    public void editProperty(Property property) throws Exception {
        try {
            entityManager.merge(property);
        } catch (Exception ex) {

        }
    }

    @Override
    public List<Property> searchPropertyByBudget(double budget) throws Exception {
        // complete this method using Criteria API
    	// CriteriaBuilder is an interface that plays the role of a factory for all individual parts of a query.
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
    	
    	// Create a type-safe criteria query that stores the information 
    	// about the tasks the query tries to achieve with a specified result type (Property class)
    	CriteriaQuery cQuery = builder.createQuery(Property.class);
    	
    	// Obtain a query root, which specifies the domain objects on which the query is evaluated.
    	Root<Property> root = cQuery.from(Property.class);
    	cQuery.select(root);
    	
    	// Construct the criteria for filtering entity instance as needed
    	Predicate predicate = builder.le(root.get("price").as(Double.class), budget);    	
    	cQuery.where(predicate);
    	
    	List<Property> propertyList = entityManager.createQuery(cQuery).getResultList();
    	
        return propertyList;
    }
}
