package calculate;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import calculate.client.WebServiceClient;
import java.io.Serializable;

@Named(value = "webServiceManagedBean")
@SessionScoped
public class WebServiceManagedBean implements Serializable {
	private WebServiceClient webServiceClient;
	private double principle;
	private int year;
	private double interestRate;
	private double monthlyPayment;

	public String calculateMonthlyPayment() {
		webServiceClient = new WebServiceClient();
		monthlyPayment = webServiceClient.calculateMonthlyPayment(principle, year, interestRate);
		return "index";
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getMonthlyPayment() {
		return monthlyPayment;
	}

	public void setMonthlyPayment(double monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
}
