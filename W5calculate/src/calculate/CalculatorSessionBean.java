package calculate;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class CalculatorSessionBean
 * EJB class
 */
@Singleton
@LocalBean
public class CalculatorSessionBean {

    /**
     * Default constructor. 
     */
    public CalculatorSessionBean() {
        // TODO Auto-generated constructor stub
    }
    
    // Method to calculator the monthly payment
	public Double calculateMonthlyPayment(double principle, int year, double interestRate) {
		Double monthlyPayment = 0.0;
		int numberOfPayments = year * 12;
		monthlyPayment = principle * (interestRate * (Math.pow((1 + interestRate), numberOfPayments)))
				/ ((Math.pow((1 + interestRate), numberOfPayments)) - 1);
		return monthlyPayment;
	}

}
