package calculate.client;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class WebServiceClient {
	private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://0.0.0.0:8080/W5calculate/webresources";

    public WebServiceClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("calculator");
    }

    public String getHtml() throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.TEXT_HTML).get(String.class);
    }

    public void setPostName() throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED).post(null);
    }
    
    
    public double calculateMonthlyPayment(Double principle,Integer year, Double interestRate) throws ClientErrorException {
       double monthlyPayment = 0.0;
       // Path of the getCalculateResult defined in Calculator
       // @Path("getCalculateResult/{principle}/{year}/{interestRate}")
       webTarget = client.target(BASE_URI).path("calculator/"+"getCalculateResult/"+ principle.toString() + "/" + year.toString() + "/" + interestRate.toString());
       // Get result through request
       String result = webTarget.request(javax.ws.rs.core.MediaType.TEXT_HTML).get(String.class);
       // String to Double
       monthlyPayment = Double.parseDouble(result);
     
       return monthlyPayment;
    }
    
    
    public void putHtml(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.TEXT_HTML).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.TEXT_HTML));
    }
    
    public void close() {
        client.close();
    }
}
