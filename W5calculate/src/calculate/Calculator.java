package calculate;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("calculator")
public class Calculator {
    @SuppressWarnings("unused")
    @Context
    private UriInfo context;
    
    @EJB
    private CalculatorSessionBean calculatorSBean;

    /**
     * Default constructor. 
     */
    public Calculator() {
        // TODO Auto-generated constructor stub
    }

    /**
     * GET method for 
     * @param principle
     * @param year
     * @param interestRate
     * @return
     */
    @GET
    @Path("getCalculateResult/{principle}/{year}/{interestRate}")
    @Produces("text/html")
    public String getCalculateResult(@PathParam("principle") double principle,
    		@PathParam("year") int year,
    		@PathParam("interestRate") double interestRate) {
    	
       return calculatorSBean.calculateMonthlyPayment(principle,year,interestRate).toString();
    }

    /**
     * PUT method for updating or creating an instance of Calculator
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/html")
    public void putHtml(String content) {
    }

}