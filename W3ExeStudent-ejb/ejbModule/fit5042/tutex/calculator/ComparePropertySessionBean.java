package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty {
	
	
    private Set<Property> propertyList;
    
    public ComparePropertySessionBean() {
    	propertyList = new HashSet<>();
    }
    
    @Override
    public void addProperty(Property property) {
    	propertyList.add(property);
    }
    
    @Override
    public void removeProperty(Property property) {
        for (Property p : propertyList ) {
        	if (p.getPropertyId() == property.getPropertyId()) {
        		propertyList.remove(p);
        		break;
        	}
        }
    }    

    @Override
    public int getBestPerRoom() {
        
        int numberOfRooms;
        double bestPerRoom=100000000.00;
        double price;
        
        Integer bestPropertyID = 0;
        
        for(Property property : propertyList)
        {
            numberOfRooms = property.getNumberOfBedrooms();
            price = property.getPrice();
            if(price / numberOfRooms < bestPerRoom)
            {
                bestPerRoom = price / numberOfRooms;
                bestPropertyID = property.getPropertyId();
            }
        }
        return bestPropertyID;
    }

    /**
     * 
     * @return 
     * @throws javax.ejb.CreateException
     * @throws java.rmi.RemoteException
     */
    @PostConstruct
    public void init() {
    	propertyList = new HashSet<>();
    }

    public CompareProperty create() throws CreateException, RemoteException {
        return null;
    }

    public void ejbCreate() throws CreateException {
    }

}
